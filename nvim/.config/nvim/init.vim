call plug#begin()
Plug 'tpope/vim-fireplace', { 'for': 'clojure' }
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'tpope/vim-commentary'
Plug 'tombert/vim-wombat-scheme'
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
call plug#end()
let g:deoplete#enable_at_startup = 1

nnoremap <C-P> :Files<CR>
nnoremap <C-S> :Rg<CR>
:set expandtab

colorscheme wombat
