{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs";
    hello-thing.url = "github:Tombert/hello-flake";
    hello-thing.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = { self, nixpkgs, hello-thing }: {
    packages."aarch64-darwin".default =
      let pkgs = nixpkgs.legacyPackages."x86_64-darwin";
      in pkgs.buildEnv {
        name = "home-packages";
        paths = [
          pkgs.git
          pkgs.nixfmt
          pkgs.neovim
          pkgs.tmux
          pkgs.stow
          pkgs.gnupg
          pkgs.wget
          pkgs.parallel
          pkgs.pass
          pkgs.fzf
          pkgs.ripgrep
        ];
      };
  };

}
